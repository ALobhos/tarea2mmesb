import time

PATTERNS = ["CTA", "CTT", "CTG", "CTC", "TAG", "AAG", "CAG", "GAG"]
ALPHABET = "ATCG"

def allBaseDependencyMethod(data, invert=False):

    start = time.time()
    segments = data.count(" ") + 1
    prediction = {"CTA": 0, "CTT": 0, "CTG": 0, "CTC": 0,
                  "TAG": 0, "AAG": 0, "CAG": 0, "GAG": 0}

    triplets = {}
    baseCount = {}
    duplets = {}
    baseFrequency = {}
    dupletFrequency = {}
    tripletFrequency = {}

    # Generate all three dictionaries
    for i in ALPHABET:
        baseCount[i] = 0
        for j in ALPHABET:
            duplets[i+j] = 0
            for k in ALPHABET:
                triplets[i+j+k] = 0

    # Count for every apereance using a sliding window only checking the 
    # data array once
    for base in range(len(data)):

        if data[base] in ALPHABET:
            baseCount[data[base]] += 1

            if (base+1) < len(data) and data[base+1] in ALPHABET:
                duplets[data[base]+data[base+1]] += 1

                if (base+2) < len(data) and data[base+2] in ALPHABET:
                    triplets[data[base]+data[base+1]+data[base+2]] += 1

    # Calculate proportions and dependencies between combinations
    if invert:
        for i in ALPHABET:
            baseFrequency[i] = baseCount[i] / (len(data) - (segments-1))

            for j in ALPHABET:
                dupletFrequency[i+j] = duplets[i+j] / baseCount[j]

                for k in ALPHABET:
                    tripletFrequency[i+j+k] = triplets[i+j+k] / duplets[j+k]

    else:
        for i in ALPHABET:
            baseFrequency[i] = baseCount[i] / (len(data) - (segments-1))

            for j in ALPHABET:
                dupletFrequency[i+j] = duplets[i+j] / baseCount[i]

                for k in ALPHABET:
                    tripletFrequency[i+j+k] = triplets[i+j+k] / duplets[i+j]

    # Estimate ocurrences of every pattern
    if invert: 
        for patt in PATTERNS:
            pred = len(data)-(3*segments)+segments
            pred = pred * baseFrequency[patt[2]]
            pred = pred * dupletFrequency[patt[1:]]
            pred = pred * tripletFrequency[patt]
            prediction[patt] = pred
    else:
        for patt in PATTERNS:
            pred = len(data)-(3*segments)+segments
            pred = pred * baseFrequency[patt[0]]
            pred = pred * dupletFrequency[patt[0:2]]
            pred = pred * tripletFrequency[patt]
            prediction[patt] = pred

    total = time.time() - start
    return {"Method": "All Composition",
            "Prediction": prediction,
            "Time": total}


def markovMethod(data, invert=False):

    start = time.time()
    segments = data.count(" ") + 1
    prediction = {"CTA": 0, "CTT": 0, "CTG": 0, "CTC": 0,
                  "TAG": 0, "AAG": 0, "CAG": 0, "GAG": 0}
    baseCount = {}
    duplets = {}
    baseFrequency = {}
    dupletFrequency = {}

    # Generate dictionaries for combinations of duplets
    for i in ALPHABET:
        baseCount[i] = 0
        for j in ALPHABET:
            duplets[i+j] = 0

    # Count every duplet and single base in one pass
    for base in range(len(data)):

        if data[base] in ALPHABET:
            baseCount[data[base]] += 1

            if (base+1) < len(data) and data[base+1] in ALPHABET:
                duplets[data[base]+data[base+1]] += 1

    # Calculate dependencies and transitions depending of use case 
    # (inverted or not)
    if invert:
        for i in ALPHABET:
            baseFrequency[i] = baseCount[i] / (len(data) - (segments-1))

            for j in ALPHABET:
                dupletFrequency[i+j] = duplets[i+j] / baseCount[j]

    else:
        for i in ALPHABET:
            baseFrequency[i] = baseCount[i] / (len(data) - (segments-1))

            for j in ALPHABET:
                dupletFrequency[i+j] = duplets[i+j] / baseCount[i]

    # Compute and estimate number of espected ocurrences of every pattern
    if invert:
        for patt in PATTERNS:
            pred = (len(data)-(3*segments)+segments) * baseFrequency[patt[2]]
            pred = pred * dupletFrequency[patt[1:]]
            pred = pred * dupletFrequency[patt[0:2]]
            prediction[patt] = pred

    else:
        for patt in PATTERNS:
            pred = (len(data)-(3*segments)+segments) * baseFrequency[patt[0]]
            pred = pred * dupletFrequency[patt[0:2]]
            pred = pred * dupletFrequency[patt[1:]]
            prediction[patt] = pred

    total = time.time() - start

    return {"Method": "Markov dependency",
            "Prediction": prediction,
            "Time": total}


# Evaluating base composition only
def compositionInfoMethod(data):

    start = time.time()
    segments = data.count(" ") + 1
    prediction = {"CTA": 0, "CTT": 0, "CTG": 0, "CTC": 0,
                  "TAG": 0, "AAG": 0, "CAG": 0, "GAG": 0}

    baseCount = {"A": 0, "T": 0, "C": 0, "G": 0}
    
    # Count all aparitions of every base and calculate the composition
    for base in data:
        if base in ALPHABET:
            baseCount[base] += 1

    composition = {"A": 0, "T": 0, "C": 0, "G": 0}

    for base in composition:
        composition[base] = baseCount[base] / (len(data) - (segments-1))

    for patt in PATTERNS:
        pred = ((len(data))-(3*segments)+segments)
        pred = pred * composition[patt[0]]
        pred = pred * composition[patt[1]]
        pred = pred * composition[patt[2]]
        prediction[patt] = pred
            

    total = time.time() - start

    return {"Method": "Composition Info",
            "Prediction": prediction,
            "Time": total}

# Simplest method
def noInfoMethod(data):

    start = time.time()
    prediction = {"CTA": 0, "CTT": 0, "CTG": 0, "CTC": 0,
                  "TAG": 0, "AAG": 0, "CAG": 0, "GAG": 0}
    segments = data.count(" ") + 1

    # For every segment, the usable section is:
    # total lenght - pattern legnth + 1
    for patt in prediction:
        prediction[patt] = (len(data)-(segments*3)+segments) / (4**3)
    total = time.time() - start

    return {"Method": "No Composition Info",
            "Prediction": prediction,
            "Time": total}

